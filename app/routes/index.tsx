import { useFetcher } from '@remix-run/react'
import { useEffect } from 'react'

export default function Index() {
  const fetcher = useFetcher()
  useEffect(() => {
    if (fetcher.state === 'idle' && fetcher.type === 'init') {
      fetcher.load('/data')
    }
  }, [fetcher.state, fetcher.type])

  return (
    <main>
      <p>state: {fetcher.state}</p>
      <p>type: {fetcher.type}</p>
    </main>
  )
}
